from abc import ABC, abstractmethod
from tkinter.filedialog import askopenfile
from tkinter.messagebox import showerror
from typing import List

from geometry import Rectangle


class DataReader(ABC):
    @abstractmethod
    def read(self) -> List[Rectangle]:
        raise NotImplementedError


class FileDataReader(DataReader):
    @staticmethod
    @abstractmethod
    def _get_file():
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def _extract_data(file):
        raise NotImplementedError

    def read(self):
        file = self._get_file()
        if not file:
            return None
        with file:
            data = self._extract_data(file)
        return data


class AskOpenFileMixin:
    @staticmethod
    def _get_file():
        return askopenfile()


class ExtractCsvDataMixin:
    @staticmethod
    def _extract_data(file):
        import csv

        try:
            csv_reader = csv.reader(file)
            data = [Rectangle(int(width), int(height)) for width, height in csv_reader]
        except AttributeError:
            data = None
        except ValueError:
            showerror(message='Требуется файл формата CSV')
            data = None
        return data


class CsvDataReader(AskOpenFileMixin, ExtractCsvDataMixin, FileDataReader):
    pass


class OpenPredefinedFileMixin:
    def __init__(self, file_name):
        self.file_name = file_name

    def _get_file(self):
        return open(self.file_name)


class TestDataReader(OpenPredefinedFileMixin, ExtractCsvDataMixin, FileDataReader):
    pass


class RandomDataReader(DataReader):
    def read(self) -> List[Rectangle]:
        from random import randint
        big_rects = [
            Rectangle(randint(80, 100), randint(140, 200), 0, 0)
            for _ in range(randint(5, 10))
        ]
        small_rects = [
            Rectangle(randint(20, 60), randint(20, 60), 0, 0)
            for _ in range(randint(20, 25))
        ]
        return big_rects + small_rects
