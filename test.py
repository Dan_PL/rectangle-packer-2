import os
import pathlib
from dataclasses import dataclass
from datetime import datetime
from statistics import mean
from typing import List

import matplotlib.pyplot as ppt

from algorithms import GALDAdjustedAlgorithm, ParallelizedSearch
from controllers import MergedControllersBatch, TimeController, QualityController
from datareaders import TestDataReader


class TestCase:
    @dataclass
    class TestResults:
        elapsed_time: float
        iterations_number: int
        fitness_value: float
        fitness_history: List[float]

    PLOT_LABELS = {
        'x': 'Итерации',
        'y': 'Целевая функция'
    }
    PLOTS_DIR = 'plots'

    def __init__(self, data_file, time_limit=None, target_quality=None, runs=100, show_plots=False):
        if not (time_limit or target_quality):
            raise ValueError('Neither time limit nor target quality is supplied')
        if not data_file:
            raise ValueError('Initial data file name is not provided')
        self.initial_data = TestDataReader(data_file).read()
        self.time_limit = None
        self.target_value = None
        self.controller_classes = []
        self.controller_params = {}
        if time_limit:
            self.time_limit = time_limit
            self.controller_classes.append(TimeController)
            self.controller_params['time_limit'] = time_limit
        if target_quality:
            self.target_quality = target_quality
            self.controller_classes.append(QualityController)
            self.controller_params['target_quality'] = target_quality
        self.runs = runs
        self.show_plots = show_plots

    def _create_controller(self):
        child_controllers = [controller_class() for controller_class in self.controller_classes]
        controller = MergedControllersBatch(child_controllers)
        controller.set_params(self.controller_params)
        return controller

    def test(self, algorithm_class, params, parallelized=False):
        elapsed_time_data = []
        iteration_data = []
        fitness_value_data = []
        global_time_start = datetime.now()
        fitness_history = None
        print(f'Start testing {algorithm_class.NAME}')
        for i in range(1, self.runs + 1):
            print(f'Test #{i}')
            if parallelized:
                algorithm = ParallelizedSearch(algorithm_class, self.initial_data, params)
            else:
                algorithm = algorithm_class(self.initial_data, params)
            controller = self._create_controller()
            test_data = self.run_test(algorithm, controller)
            elapsed_time_data.append(test_data.elapsed_time)
            iteration_data.append(test_data.iterations_number)
            fitness_value_data.append(test_data.fitness_value)
            if i == self.runs:
                fitness_history = test_data.fitness_history
        total_elapsed_time = datetime.now() - global_time_start
        print(f'AVERAGE TIME {mean(elapsed_time_data)}')
        print(f'AVERAGE STEPS NUMBER {mean(iteration_data)}')
        print(f'AVERAGE MAX FITNESS VALUE {mean(fitness_value_data)}')
        print(f'TOTAL TESTING TIME {total_elapsed_time}')

        x = list(range(1, len(fitness_history) + 1))
        fig, ax = ppt.subplots()
        ax.plot(x, fitness_history)
        ax.set(xlabel=self.PLOT_LABELS['x'], ylabel=self.PLOT_LABELS['y'], title=algorithm_class.NAME)
        plots_dir_name = os.path.join(pathlib.Path().absolute(), self.PLOTS_DIR)
        if not os.path.exists(plots_dir_name):
            os.mkdir(plots_dir_name)
        plot_file_name = f'{algorithm_class.NAME}-target={self.target_value}-limit={self.time_limit}'
        plot_file_path = os.path.join(plots_dir_name, plot_file_name)
        ppt.savefig(plot_file_path)
        if self.show_plots:
            ppt.show()

    @classmethod
    def run_test(cls, algorithm, controller):
        iteration_counter = 0
        time_start = datetime.now()
        solution = None
        controller.handle_start()
        fitness_history = []
        while not controller.finished():
            iteration_counter += 1
            solution = algorithm.next_solution()
            controller.add_solution(solution)
            fitness_history.append(solution.fitness_value)
            print(f'Step {iteration_counter}')
        algorithm.terminate()
        controller.handle_finish()
        elapsed_time = datetime.now() - time_start
        results = cls.TestResults(
            elapsed_time.total_seconds(),
            iteration_counter,
            solution.fitness_value,
            fitness_history
        )
        return results


if __name__ == '__main__':
    TEST_FILE = 'test_data.csv'
    ALGORITHMS_PARAMS = {
        'population_size': 5,
        'number_of_children': 5,
        'number_of_mutants': 1,
        'start_temperature': 1,
        'stop_temperature': 0.001,
        'cooling_coefficient': 0.8,
        'temperature_level_size': 10,
        'stagnation_limit': 5,
        'wall': 1000,
        'floor': 1000
    }
    test_case = TestCase(TEST_FILE, time_limit=10, target_quality=0.98, runs=1)
    test_case.test(GALDAdjustedAlgorithm, ALGORITHMS_PARAMS, parallelized=True)
    test_case.test(GALDAdjustedAlgorithm, ALGORITHMS_PARAMS)
