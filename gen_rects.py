from random import choice, randint
from sys import argv


def gen_cuts(side_size, num_of_cuts):
    if num_of_cuts == 0:
        return [randint(1, side_size)]
    else:
        cuts = gen_cuts(side_size, num_of_cuts-1)
        cuts.append(randint(1, side_size - cuts[-1]))
        return cuts


def main():
    width = int(argv[1])
    height = int(argv[2])
    amount = int(argv[3])
    delimiters = [i for i in range(1, amount) if amount % i == 0]
    num_of_cuts1 = choice(delimiters)
    num_of_cuts2 = amount // num_of_cuts1
    cuts1 = gen_cuts(width, num_of_cuts1 - 1)
    cuts2 = gen_cuts(height, num_of_cuts2 - 1)
    rects = [(i, j) for i in cuts1 for j in cuts2]

    lines = [f'{rect[0]},{rect[1]}\n' for rect in rects]
    with open('test_data.csv', 'w') as file:
        file.writelines(lines)


if __name__ == '__main__':
    main()
