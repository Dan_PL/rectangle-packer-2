from abc import ABC, abstractmethod
from copy import copy
from math import exp
from multiprocessing import Process, Queue, cpu_count
from random import choice, sample, random

from solutions import SimpleGeneticSolution


class Algorithm(ABC):
    NAME = None
    parameters = {}

    @classmethod
    def get_params_meta(cls):
        return cls.parameters

    def __init__(self, initial_data, params):
        self.rectangles = initial_data
        for param_name in self.get_params_meta():
            self.__setattr__(param_name, params[param_name])

    @abstractmethod
    def next_solution(self):
        raise NotImplementedError

    def terminate(self):
        pass


class GeneticAlgorithm(Algorithm):
    NAME = 'Генетический алгоритм'
    parameters = {
        'population_size': {
            'type': int,
            'comment': 'Размер популяции',
            'default_value': 30
        },
        'number_of_children': {
            'type': int,
            'comment': 'Число потомков',
            'default_value': 10
        },
        'number_of_mutants': {
            'type': int,
            'comment': 'Число мутантов',
            'default_value': 5
        },
        'wall': {
            'type': int,
            'comment': 'Ограничение по X',
            'default_value': 500
        },
        'floor': {
            'type': int,
            'comment': 'Ограничение по Y',
            'default_value': 500
        },
    }
    solution_class = SimpleGeneticSolution

    def __init__(self, initial_data, params, initial_solution=None):
        super().__init__(initial_data, params)
        self.population = [
            self.solution_class(rectangles=self.rectangles, wall=self.wall, floor=self.floor)
            for _ in range(self.population_size)
        ]
        if initial_solution:
            self.population[0] = initial_solution

    def next_solution(self):
        pairs_for_crossover = [sample(self.population, 2) for _ in range(self.number_of_children)]
        for pair in pairs_for_crossover:
            self.population.append(self.solution_class(parents=pair))
        self.population += [
            choice(self.population).get_mutant()
            for _ in range(self.number_of_mutants)
        ]
        self.population.sort(key=lambda solution: solution.fitness_value, reverse=True)
        self.population = self.population[:self.population_size]
        return self.population[0]


class SimulatedAnnealing(Algorithm):
    NAME = 'Имитация отжига'
    parameters = {
        'start_temperature': {
            'type': float,
            'comment': 'Стартовая температура',
            'default_value': 1
        },
        'stop_temperature': {
            'type': float,
            'comment': 'Конечная температура',
            'default_value': 0.001
        },
        'cooling_coefficient': {
            'type': float,
            'comment': 'Коэффициент охлаждения',
            'default_value': 0.95
        },
        'temperature_level_size': {
            'type': int,
            'comment': 'Длина температурного уровня',
            'default_value': 10
        },
        'wall': {
            'type': int,
            'comment': 'Ограничение по X',
            'default_value': 500
        },
        'floor': {
            'type': int,
            'comment': 'Ограничение по Y',
            'default_value': 500
        },
    }

    def __init__(self, initial_data, params, initial_solution=None):
        super().__init__(initial_data, params)
        if initial_solution:
            self.solution = initial_solution
        else:
            self.solution = SimpleGeneticSolution(
                rectangles=self.rectangles,
                wall=self.wall,
                floor=self.floor
            )
        self.temperature = self.start_temperature
        self.best_solution = self.solution

    def next_solution(self):
        new_solution = self.solution
        for _ in range(self.temperature_level_size):
            new_solution = self.solution.get_mutant()
            delta = new_solution.fitness_value - self.solution.fitness_value
            if delta > 0:
                self.solution = new_solution
            else:
                move_probability = exp(delta / self.temperature)
                if random() <= move_probability:
                    self.solution = new_solution
        self.temperature *= self.cooling_coefficient
        if new_solution.fitness_value > self.best_solution.fitness_value:
            self.best_solution = new_solution
        if self.temperature < self.stop_temperature:
            self.best_solution.mark_as_terminal()
        return self.best_solution


class LocalDescent(Algorithm):
    NAME = 'Алгоритм локального спуска'
    parameters = {
        'wall': {
            'type': int,
            'comment': 'Ограничение по X',
            'default_value': 500
        },
        'floor': {
            'type': int,
            'comment': 'Ограничение по Y',
            'default_value': 500
        },
    }

    def __init__(self, initial_data, params, initial_solution=None):
        super().__init__(initial_data, params)
        if initial_solution:
            self.solution = initial_solution
        else:
            self.solution = SimpleGeneticSolution(
                rectangles=self.rectangles,
                wall=self.wall,
                floor=self.floor
            )

    def next_solution(self):
        for i in range(len(self.solution.order)):
            for j in range(i + 1, len(self.solution.order)):
                new_solution = SimpleGeneticSolution()
                new_order = copy(self.solution.order)
                new_order[i], new_order[j] = new_order[j], new_order[i]
                new_solution.order = new_order
                new_solution.generate_new(
                    self.solution.rectangles,
                    self.solution.wall,
                    self.solution.floor
                )
                if new_solution.fitness_value > self.solution.fitness_value:
                    self.solution = new_solution
                    return new_solution
        self.solution.mark_as_terminal()
        return self.solution


class TwoStageAlgorithm(Algorithm):
    first_algorithm_class = None
    second_algorithm_class = None

    parameters = {}

    INITIAL_SEARCH_STATE = 0
    FINAL_SEARCH_STATE = 1

    stagnation_limit = 15

    @classmethod
    def get_params_meta(cls):
        if not cls.parameters:
            cls.parameters.update(cls.first_algorithm_class.get_params_meta())
            cls.parameters.update(cls.second_algorithm_class.get_params_meta())
        return cls.parameters

    def __init__(self, initial_data, params):
        super().__init__(initial_data, params)
        self.params = params
        self.first_algorithm = self.first_algorithm_class(initial_data, params)
        self.second_algorithm = None
        self.stagnation_counter = 0
        self.last_fitness_value = -1.0
        self.state = self.INITIAL_SEARCH_STATE

    def next_solution(self):
        if self.state == self.INITIAL_SEARCH_STATE:
            new_solution = self.first_algorithm.next_solution()
            if new_solution.fitness_value == self.last_fitness_value:
                self.stagnation_counter += 1
            else:
                self.stagnation_counter = 0
            if self.stagnation_counter > self.stagnation_limit:
                self.second_algorithm = self.second_algorithm_class(self.rectangles, self.params, new_solution)
                self.state = self.FINAL_SEARCH_STATE
                print(f'Switching algorithm to {self.second_algorithm_class.NAME}')
            self.last_fitness_value = new_solution.fitness_value
        else:
            new_solution = self.second_algorithm.next_solution()
        return new_solution


class GASATwoStageAlgorithm(TwoStageAlgorithm):
    NAME = 'Гибридный алгоритм (ГА + ИО)'
    first_algorithm_class = GeneticAlgorithm
    second_algorithm_class = SimulatedAnnealing


class AdjustedAlgorithm(Algorithm):
    first_algorithm_class = None
    second_algorithm_class = None

    parameters = {
        'stagnation_limit': {
            'type': int,
            'comment': 'Допустимая стагнация',
            'default_value': 5
        },
    }

    SEARCH_STATE = 0
    ADJUSTMENT_STATE = 1

    @classmethod
    def get_params_meta(cls):
        params = {}
        params.update(cls.parameters)
        params.update(cls.first_algorithm_class.get_params_meta())
        params.update(cls.second_algorithm_class.get_params_meta())
        return params

    def __init__(self, initial_data, params):
        super().__init__(initial_data, params)
        self.params = params
        self.first_algorithm = self.first_algorithm_class(initial_data, params)
        self.second_algorithm = None
        self.stagnation_counter = 0
        self.last_fitness_value = -1.0
        self.state = self.SEARCH_STATE

    def next_solution(self):
        if self.state == self.SEARCH_STATE:
            new_solution = self.first_algorithm.next_solution()
            if new_solution.fitness_value == self.last_fitness_value:
                self.stagnation_counter += 1
            else:
                self.stagnation_counter = 0
            if self.stagnation_counter > self.stagnation_limit or new_solution.is_terminal():
                self.second_algorithm = self.second_algorithm_class(self.rectangles, self.params, new_solution)
                self.state = self.ADJUSTMENT_STATE
                print(f'Switching algorithm to {self.second_algorithm_class.NAME}')
                self.stagnation_counter = 0
            self.last_fitness_value = new_solution.fitness_value
        else:
            new_solution = self.second_algorithm.next_solution()
            if new_solution.fitness_value == self.last_fitness_value:
                self.stagnation_counter += 1
            else:
                self.stagnation_counter = 0
            if self.stagnation_counter > self.stagnation_limit or new_solution.is_terminal():
                self.first_algorithm = self.first_algorithm_class(self.rectangles, self.params, new_solution)
                self.state = self.SEARCH_STATE
                print(f'Switching algorithm to {self.first_algorithm_class.NAME}')
                self.stagnation_counter = 0
            self.last_fitness_value = new_solution.fitness_value
        new_solution._is_terminal = False
        return new_solution


class GALDAdjustedAlgorithm(AdjustedAlgorithm):
    NAME = 'Гибридный алгоритм (ГА + ЛС)'
    first_algorithm_class = GeneticAlgorithm
    second_algorithm_class = LocalDescent


class SALDAdjustedAlgorithm(AdjustedAlgorithm):
    NAME = 'Гибридный алгоритм (ИО + ЛС)'
    first_algorithm_class = SimulatedAnnealing
    second_algorithm_class = LocalDescent


class ParallelizedSearch(Algorithm):
    ATTEMPTS_BEFORE_RETURN = 5

    def __init__(self, algorithm_class, initial_data, params):
        super().__init__(initial_data, params)
        import settings
        self.queue = Queue()
        self.best_solution = None
        self.workers_num = settings.NUM_OF_WORKERS or cpu_count()
        self.workers = []
        for worker_idx in range(self.workers_num):
            process = Process(
                name=f'worker_{worker_idx}',
                target=self._worker_task,
                args=(algorithm_class, self.rectangles, params, self.queue)
            )
            self.workers.append(process)
            process.start()
        self.searching = True

    @staticmethod
    def _worker_task(algorithm_class, initial_data, params, queue):
        algorithm = algorithm_class(initial_data, params)
        while True:
            solution = algorithm.next_solution()
            queue.put(solution)

    def terminate(self):
        for worker in self.workers:
            worker.terminate()

    def next_solution(self):
        self.searching = True
        attempts_count = 0
        while self.searching and attempts_count < self.ATTEMPTS_BEFORE_RETURN:
            attempts_count += 1
            solution = self.queue.get()
            if not self.best_solution or solution.fitness_value > self.best_solution.fitness_value:
                self.searching = False
                self.best_solution = solution
        return self.best_solution
