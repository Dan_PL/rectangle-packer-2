from algorithms import (
    GeneticAlgorithm, SimulatedAnnealing, LocalDescent, GASATwoStageAlgorithm, GALDAdjustedAlgorithm,
    SALDAdjustedAlgorithm
)
from controllers import TimeController, IterationController, QualityController, TerminalSolutionController
from datareaders import CsvDataReader, RandomDataReader

DATAREADER = CsvDataReader
RANDOM_DATAREADER = RandomDataReader

ALGORITHMS = [
    GeneticAlgorithm,
    SimulatedAnnealing,
    GASATwoStageAlgorithm,
    LocalDescent,
    GALDAdjustedAlgorithm,
    SALDAdjustedAlgorithm
]
CONTROLLERS = [TimeController, IterationController, QualityController, TerminalSolutionController]

SOLUTION_RENDERERS = []

PARALLELIZED = True
NUM_OF_WORKERS = None
