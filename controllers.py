from abc import ABC, abstractmethod
from datetime import datetime, timedelta
from typing import Sequence


class Controller(ABC):
    def set_params(self, params):
        for param_name in params:
            self.__setattr__(param_name, params[param_name])

    def handle_start(self):
        pass

    def add_solution(self, solution):
        pass

    @abstractmethod
    def finished(self):
        raise NotImplementedError

    def handle_finish(self):
        pass


class MergedControllersBatch(Controller):
    def __init__(self, controllers: Sequence[Controller]):
        super().__init__()
        self.controllers = controllers
        self.parameters = {}
        for controller in controllers:
            self.parameters.update(controller.parameters)

    def set_params(self, params):
        for controller in self.controllers:
            controller.set_params(params)

    def handle_start(self):
        for controller in self.controllers:
            controller.handle_start()

    def add_solution(self, solution):
        for controller in self.controllers:
            controller.add_solution(solution)

    def finished(self):
        return any(controller.finished() for controller in self.controllers)

    def handle_finish(self):
        for controller in self.controllers:
            controller.handle_finish()


class TimeController(Controller):
    parameters = {
        'time_limit': {
            'type': int,
            'comment': 'Время работы (с.)',
            'default_value': 20
        },
    }

    def __init__(self):
        super().__init__()
        self.start_time = None
        self.time_limit = None

    def handle_start(self):
        assert self.time_limit, 'Не задано время работы!'
        self.start_time = datetime.now()

    def finished(self):
        return (datetime.now() - self.start_time) > timedelta(seconds=self.time_limit)


class IterationController(Controller):
    parameters = {
        'iteration_limit': {
            'type': int,
            'comment': 'Число итераций',
            'default_value': 100
        },
    }

    def __init__(self):
        super().__init__()
        self.counter = None
        self.iteration_limit = None

    def handle_start(self):
        assert self.iteration_limit, 'Не задано число итераций'
        self.counter = 0

    def add_solution(self, solution):
        self.counter += 1

    def finished(self):
        return self.counter >= self.iteration_limit


class QualityController(Controller):
    parameters = {
        'target_quality': {
            'type': float,
            'comment': 'Качество решения',
            'default_value': 0.9
        },
    }

    def __init__(self):
        super().__init__()
        self.target_quality = None
        self.reached_quality = None

    def handle_start(self):
        assert self.target_quality, 'Не задано требуемое качество решения'
        self.reached_quality = 0.0

    def add_solution(self, solution):
        self.reached_quality = solution.fitness_value

    def finished(self):
        return self.reached_quality >= self.target_quality


class TerminalSolutionController(Controller):
    parameters = {}

    def __init__(self):
        super().__init__()
        self.is_finished = False

    def add_solution(self, solution):
        if solution.is_terminal():
            self.is_finished = True

    def finished(self):
        return self.is_finished
