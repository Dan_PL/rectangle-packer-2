from datetime import datetime
from tkinter import (
    Frame, END, SINGLE, Listbox, Y, LEFT, Button, RIGHT, ANCHOR, Canvas, Toplevel, Checkbutton, BOTTOM, IntVar
)
from tkinter.messagebox import showerror, showinfo
from tkinter.simpledialog import Dialog, Label, Entry

import settings
from algorithms import ParallelizedSearch
from controllers import MergedControllersBatch


class MainWindow(Frame):
    def __init__(self, master, *args, **kwargs):
        self.master = master
        self.data_reader_class = settings.DATAREADER
        self.random_data_reader_class = settings.RANDOM_DATAREADER
        self.algorithm_classes = {algorithm.NAME: algorithm for algorithm in settings.ALGORITHMS}
        self.controllers = [Controller() for Controller in settings.CONTROLLERS]
        self.renderer_classes = settings.SOLUTION_RENDERERS

        self.algorithms_list = Listbox(master, selectmode=SINGLE, width=30)
        for name in self.algorithm_classes:
            self.algorithms_list.insert(END, name)
        self.algorithms_list.pack(side=LEFT, fill=Y, padx=10, pady=10)

        self.start_button = Button(master, text='СТАРТ', width=20, height=3, command=self._start_handler)
        self.start_button.pack(side=RIGHT, padx=20, pady=20)

        self.use_random_data = IntVar()
        self.random_data_checkbox = Checkbutton(master, text="Случайные данные", variable=self.use_random_data)
        self.random_data_checkbox.pack(side=BOTTOM, pady=20)

        super().__init__(master, *args, **kwargs)

    def _start_handler(self):
        if not self.algorithms_list.curselection():
            showerror(message='Выберите алгоритм')
            return
        algorithm_class = self.algorithm_classes[self.algorithms_list.get(ANCHOR)]
        if self.use_random_data.get():
            self.data_reader = self.random_data_reader_class()
        else:
            self.data_reader = self.data_reader_class()
        initial_data = self.data_reader.read()
        if not initial_data:
            return

        algorithm_params = ParamsDialog(self.master, algorithm_class.get_params_meta()).result
        if not algorithm_params:
            return

        controller = MergedControllersBatch(self.controllers)
        controller_params = ParamsDialog(self.master, controller.parameters).result
        if not controller_params:
            return
        controller.set_params(controller_params)

        iteration_counter = 0
        self.set_title('Поиск решения')
        time_start = datetime.now()
        solution = None
        if settings.PARALLELIZED:
            algorithm = ParallelizedSearch(algorithm_class, initial_data, algorithm_params)
        else:
            algorithm = algorithm_class(initial_data, params=algorithm_params)
        controller.handle_start()
        while not controller.finished():
            iteration_counter += 1
            solution = algorithm.next_solution()
            controller.add_solution(solution)
        algorithm.terminate()
        controller.handle_finish()
        elapsed_time = datetime.now() - time_start
        self.set_title('Решение найдено')
        SolutionWindow(self.master, solution, elapsed_time, iteration_counter)
        for renderer_class in self.renderer_classes:
            renderer_class().render(solution)
        self.set_title()

    def set_title(self, title='Двумерная упаковка'):
        self.master.title(title)
        self.master.update()


class ParamsDialog(Dialog):
    def __init__(self, master, params, title='Параметры'):
        self.params = params
        self.entries = {}
        super().__init__(master, title)

    def body(self, master):
        focus = None
        for param_number, param in enumerate(self.params.items()):
            param_name, attributes = param
            Label(master, text=attributes['comment']).grid(row=param_number)
            self.entries[param_name] = Entry(master)
            self.entries[param_name].grid(row=param_number, column=1)
            self.entries[param_name].insert(END, attributes['default_value'])
            focus = focus or self.entries[param_name]
        return focus

    def validate(self):
        for param_name, entry in self.entries.items():
            param = self.params[param_name]
            try:
                param['type'](entry.get())
            except ValueError as e:
                showerror(message='Неверное значение параметра "{}"'.format(param['comment']))
                return False
        return True

    def apply(self):
        self.result = {}
        for param_name, entry in self.entries.items():
            param_type = self.params[param_name]['type']
            self.result[param_name] = param_type(entry.get())


class SolutionWindow(Toplevel):
    rects_color = "#f50"

    def __init__(self, master, solution, elapsed_time, iteration_counter):
        super().__init__(master)
        rectangles = solution.rectangles
        canvas = Canvas(self, width=1000, height=1000)
        for rectangle in rectangles:
            canvas.create_rectangle(rectangle.box, outline="#000", fill=self.rects_color)
        canvas.pack()
        report = f'Качество решения: {solution.fitness_value}'
        report += f'\nВремя: {elapsed_time}'
        report += f'\nИтерации: {iteration_counter}'
        showinfo(message=report)
