from tkinter import Tk

from gui import MainWindow


def main():
    root = Tk()
    root.title('Двумерная упаковка')
    root.geometry('700x300')
    MainWindow(root).pack(side="top", fill="both", expand=True)
    root.mainloop()


if __name__ == '__main__':
    main()
