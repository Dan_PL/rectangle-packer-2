from dataclasses import dataclass


@dataclass
class Point:
    x: int
    y: int


class Rectangle:
    def __init__(self, width: int, height: int, x: int = None, y: int = None):
        self.width = width
        self.height = height
        self.x = x
        self.y = y

    @property
    def box(self):
        return (
            self.x,
            self.y,
            self.x + self.width,
            self.y + self.height
        )

    def place(self, point: Point):
        self.x = point.x
        self.y = point.y

    def at(self):
        return Point(self.x, self.y)

    def __str__(self):
        return f'Rectangle({self.width},{self.height},{self.x},{self.y})'

    @property
    def vertices(self):
        return [
            Point(self.x, self.y),
            Point(self.x + self.width, self.y),
            Point(self.x, self.y + self.height),
            Point(self.x + self.width, self.y + self.height)
        ]

    @property
    def square(self):
        return self.width * self.height

    def intersects_with(self, rect):
        dx = self.width - (rect.x - self.x) if self.x <= rect.x else rect.width - (self.x - rect.x)
        dy = self.height - (rect.y - self.y) if self.y <= rect.y else rect.height - (self.y - rect.y)
        if dx < 0:
            dx = 0
        if dy < 0:
            dy = 0
        return bool(dx * dy)
