from abc import ABC, abstractmethod
from copy import deepcopy
from dataclasses import dataclass
from random import shuffle, randint
from typing import List

from geometry import Rectangle, Point


class Solution(ABC):
    class SolutionsCache:
        @dataclass
        class CachedValues:
            fitness_value: float
            rectangles: List[Rectangle]

        def __init__(self):
            self.cache = {}

        def set(self, order, fitness_value, rectangles):
            self.cache.setdefault(
                str(order),
                self.CachedValues(fitness_value, rectangles)
            )

        def get(self, order):
            self.cache.get(str(order))

    solution_cache = SolutionsCache()

    def __init__(self, rectangles=None, wall=None, floor=None):
        self.fitness_value = None
        self.order = None
        self.rectangles = None
        self.total_square = 0
        self.wall = None
        self.floor = None
        self._is_terminal = False
        if rectangles:
            self.generate_new(rectangles, wall, floor)

    def generate_new(self, rectangles, wall, floor):
        self.rectangles = deepcopy(rectangles)
        self.wall = wall
        self.floor = floor
        self.total_square = sum(rect.square for rect in self.rectangles)
        self.order = [i for i in range(len(self.rectangles))]
        shuffle(self.order)
        self._place_rectangles()

    @abstractmethod
    def _place_rectangles(self):
        raise NotImplementedError

    def mark_as_terminal(self):
        self._is_terminal = True

    def is_terminal(self):
        return self._is_terminal


class SubsequentPlacingMixin:
    def _place_rectangles(self):
        if cached_values := self.solution_cache.cache.get(str(self.order)):
            self.fitness_value = cached_values.fitness_value
            self.rectangles = cached_values.rectangles
            return
        self.rectangles[self.order[0]].place(Point(0, 0))
        min_cover_square = None  # После размещения всех фигур здесь будет полученная покрывающая площадь
        for order_number, number in enumerate(self.order[1:], start=1):
            rectangle = self.rectangles[number]
            possible_points = []
            for number_before in self.order[:order_number]:
                possible_points += self.rectangles[number_before].vertices
            best_point = None
            min_cover_square = None
            for point in possible_points:
                rectangle.place(point)
                if any(
                        self.rectangles[number_before].intersects_with(rectangle)
                        for number_before in self.order[:order_number]
                ):
                    continue
                if rectangle.box[2] > self.wall or rectangle.box[3] > self.floor:
                    continue
                max_width = 0
                max_height = 0
                for number_before in self.order[:order_number + 1]:
                    if (width := self.rectangles[number_before].box[2]) > max_width:
                        max_width = width
                    if (height := self.rectangles[number_before].box[3]) > max_height:
                        max_height = height
                square = max_width * max_height
                if not min_cover_square or (min_cover_square > square):
                    min_cover_square = square
                    best_point = point
            if not best_point:
                self.fitness_value = 0.0
                return
            self.rectangles[number].place(best_point)
        self.fitness_value = self.total_square / min_cover_square
        self.solution_cache.set(self.order, self.fitness_value, self.rectangles)


class GeneticSolution(Solution):
    def __init__(self, rectangles=None, wall=None, floor=None, parents=None):
        super().__init__(rectangles, wall, floor)
        if parents and not rectangles:
            self._cross_over(*parents)

    @abstractmethod
    def _cross_over(self, parent1, parent2):
        raise NotImplementedError

    @abstractmethod
    def mutate(self):
        raise NotImplementedError

    def get_mutant(self):
        mutant = deepcopy(self)
        mutant.mutate()
        return mutant


class CrossOverMixin:
    def _cross_over(self, parent1, parent2):
        self.rectangles = deepcopy(parent1.rectangles)
        self.wall = parent1.wall
        self.floor = parent1.floor
        self.total_square = parent1.total_square
        cut_point = randint(1, len(parent1.order) - 1)
        self.order = []
        self.order += parent1.order[:cut_point]
        for number in parent2.order:
            if number not in self.order:
                self.order.append(number)
        self._place_rectangles()


class MutateMixin:
    def mutate(self):
        cut_point = randint(0, len(self.order) - 2)
        temp = self.order[cut_point]
        self.order[cut_point] = self.order[cut_point + 1]
        self.order[cut_point + 1] = temp
        self._place_rectangles()


class SimpleGeneticSolution(SubsequentPlacingMixin, CrossOverMixin, MutateMixin, GeneticSolution):
    pass
